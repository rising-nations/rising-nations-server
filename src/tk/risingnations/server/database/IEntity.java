/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.database;

import java.io.Serializable;

import org.bson.types.ObjectId;

public interface IEntity extends Serializable {

	void setId(ObjectId id);

	ObjectId getId();

}
