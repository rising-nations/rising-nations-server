/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.database;

import java.util.List;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;

import tk.risingnations.server.support.beans.Bean;

@Bean
public interface IMongoDbService {
	abstract Datastore getDatastore();

	abstract Morphia getMorphia();

	abstract void update(IEntity entity) throws DatabaseException;

	abstract void create(IEntity entity) throws DatabaseException;

	abstract void delete(IEntity entity) throws DatabaseException;

	abstract <T extends IEntity> Query<T> query(Class<T> entity) throws DatabaseException;

	abstract <T extends IEntity> List<T> find(Query<T> query) throws DatabaseException;
}
