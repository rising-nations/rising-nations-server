/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.database;

import tk.risingnations.server.errorhandling.ProcessingException;

/**
 *
 * @author skeeks
 */
class DatabaseException extends ProcessingException {

    private static final long serialVersionUID = 1L;

	public DatabaseException() {
    }

    public DatabaseException(String message) {
        super(message);
    }

    public DatabaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
