/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.database;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.AdvancedDatastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.query.Query;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;

import tk.risingnations.server.support.beans.BEANS;
import tk.risingnations.server.support.classinventory.IClassInventoryService;
import tk.risingnations.server.support.config.CONFIG;

public class MongoDbService implements IMongoDbService {
	public static final String DATABASE_USER_PROPERTY = "mongodb.user";
	public static final String DATABASE_PASSWORD_PROPERTY = "mongodb.password";
	public static final String DATABASE_PORT_PROPERTY = "mongodb.port";
	public static final String DATABASE_HOST_PROPERTY = "mongodb.host";
	public static final String DATABASE_DBNAME_PROPERTY = "mongodb.dbname";

	private Morphia m_morphia;
	private AdvancedDatastore m_datastore;
	private MongoClient m_mongoClient;

	public MongoDbService() {
		// lazy create the connection
	}

	protected void ensureConnected() {
		if (m_datastore == null) {
			execConnect();
		}
	}

	protected void execConnect() {
		m_morphia = execCreateMorphia();
		execInitMorphia();
		m_mongoClient = execCreateMongoClient();
		m_datastore = execCreateDatastore();
		execInitDatastore();
	}

	protected MongoClient execCreateMongoClient() {
		MongoCredential credential = MongoCredential.createCredential(CONFIG.get(DATABASE_USER_PROPERTY), CONFIG.get(DATABASE_DBNAME_PROPERTY), CONFIG.get(DATABASE_PASSWORD_PROPERTY).toCharArray());
		return new MongoClient(new ServerAddress(CONFIG.get(DATABASE_HOST_PROPERTY), CONFIG.getInt(DATABASE_PORT_PROPERTY)), Arrays.asList(credential));
	}

	protected Morphia execCreateMorphia() {
		return new Morphia();
	}

	protected AdvancedDatastore execCreateDatastore() {
		// we need the "advanced" datastore for setting dynamically the
		// collection name.
		return (AdvancedDatastore) m_morphia.createDatastore(m_mongoClient, CONFIG.get(DATABASE_DBNAME_PROPERTY));
	}

	protected void execInitMorphia() {
		List<Class<?>> entityClazzes = BEANS.get(IClassInventoryService.class).getClassesWithAnnotation(Entity.class);
		m_morphia.map(entityClazzes.toArray(new Class[entityClazzes.size()]));
	}

	protected void execInitDatastore() {
		getDatastore().ensureIndexes();
		getDatastore().ensureCaps();
		getDatastore().setDefaultWriteConcern(WriteConcern.JOURNALED);
	}

	@Override
	public AdvancedDatastore getDatastore() {
		ensureConnected();
		return m_datastore;
	}

	@Override
	public Morphia getMorphia() {
		if (m_morphia == null) {
			ensureConnected();
		}
		return m_morphia;
	}

	protected MongoClient getMongoClient() {
		return m_mongoClient;
	}

	@Override
	public void update(IEntity entity) throws DatabaseException {
		ensureConnected();
		try {
			getDatastore().save(entity);
		} catch (Exception e) {
			throw new DatabaseException("could not update entity", e);
		}
	}

	@Override
	public void create(IEntity entity) throws DatabaseException {
		ensureConnected();
		try {
			Key<IEntity> key = getDatastore().save(entity);
			entity.setId((ObjectId) key.getId());
		} catch (Exception e) {
			throw new DatabaseException("could not create entity", e);
		}
	}

	@Override
	public void delete(IEntity entity) throws DatabaseException {
		ensureConnected();
		if (entity == null) {
			throw new DatabaseException("cannot delete 'null' from database");
		}
		try {
			getDatastore().delete(entity.getClass(), entity);
		} catch (Exception e) {
			throw new DatabaseException("could not delete entity", e);
		}
	}

	@Override
	public <T extends IEntity> Query<T> query(Class<T> entity) {
		ensureConnected();
		return getDatastore().createQuery(entity);
	}

	@Override
	public <T extends IEntity> List<T> find(Query<T> query) throws DatabaseException {
		ensureConnected();
		if (query == null) {
			throw new DatabaseException("no query issued");
		}

		List<T> results = query.asList();
		return results == null ? new ArrayList<T>() : results;
	}

}
