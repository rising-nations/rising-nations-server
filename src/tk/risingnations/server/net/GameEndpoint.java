package tk.risingnations.server.net;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.server.ServerEndpointConfig.Configurator;

import tk.risingnations.server.action.IActionController;
import tk.risingnations.server.auth.jwt.IJwtService;
import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.game.session.GameSession;
import tk.risingnations.server.net.GameEndpoint.GameEndpointConfigurator;
import tk.risingnations.server.net.protocol.GameProtocolRequest;
import tk.risingnations.server.net.protocol.GameProtocolResponse;
import tk.risingnations.server.support.beans.BEANS;
import tk.risingnations.server.support.json.IJsonService;
import tk.risingnations.server.support.logging.LOG;

/**
 * 
 * @author skeeks
 *
 */
@ApplicationScoped
@ServerEndpoint(value = "/game", configurator = GameEndpointConfigurator.class, subprotocols = { "game" })
public class GameEndpoint {
	public static final String AUTHENTIFICATION_REQUEST_PARAMETER = "auth";
	private GameSession m_session;

	@OnOpen
	public void onOpen(Session session, EndpointConfig config) {
		String userId = null;
		try {
			userId = getUserIdFromSession(session);
		} catch (ProcessingException e) {
			session.getAsyncRemote().sendText(BEANS.get(IJsonService.class).writeToString(new AuthFailedResponse()));
			return;
		}
		m_session = GameSession.create(userId);

		BEANS.get(IBroadcastService.class).addSession(session);
		LOG.info(session.getId() + " connected");
	}

	/**
	 * If multiple messages are incoming, they are sequentially processed.
	 * 
	 * @param session
	 * @param message
	 */
	@OnMessage
	public void onMessage(Session session, String message) {
		GameProtocolRequest request = BEANS.get(IJsonService.class).readFromString(message, GameProtocolRequest.class);
		try {
			// TODO [ske] check if logged in user is still valid
			GameSession.put(this.m_session);
			session.getAsyncRemote().sendText(BEANS.get(IActionController.class).handle(request));
			GameSession.put(null);
		} catch (ProcessingException e) {
			sendError(session, request);
			LOG.fine(e, "error in ActionController.handle execution");
		}
	}

	public void sendError(Session session, GameProtocolRequest req) {
		String error = "error processing your request";
		GameProtocolResponse response = new GameProtocolResponse(req.getId(), false, error);
		session.getAsyncRemote().sendText(BEANS.get(IJsonService.class).writeToString(response));
	}

	@OnError
	public void onError(Session session, Throwable error) {
		error.printStackTrace();
	}

	@OnClose
	public void onClose(Session session, CloseReason reason) {
		BEANS.get(IBroadcastService.class).removeSession(session);
		LOG.info(session.getId() + " disconnected");
	}

	public static class GameEndpointConfigurator extends Configurator {
		@Override
		public boolean checkOrigin(String originHeaderValue) {
			return true;
		}
	}

	private String getUserIdFromSession(Session session) {
		try {
			String jwt = getJWTToken(session);
			return BEANS.get(IJwtService.class).parseUsername(jwt);
		} catch (ProcessingException e) {
			throw new ProcessingException("authentification failed");
		}
	}

	private String getJWTToken(Session session) {
		if (session == null || session.getRequestParameterMap() == null || session.getRequestParameterMap().get(AUTHENTIFICATION_REQUEST_PARAMETER) == null) {
			throw new ProcessingException("auth parameter not found");
		}
		return session.getRequestParameterMap().get(AUTHENTIFICATION_REQUEST_PARAMETER).get(0);
	}

	public static class AuthFailedResponse implements Serializable {
		private static final long serialVersionUID = 1L;
		public boolean authFailed = true;

		public boolean isAuthFailed() {
			return authFailed;
		}
	}
}
