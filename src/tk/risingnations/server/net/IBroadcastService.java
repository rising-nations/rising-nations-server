package tk.risingnations.server.net;

import javax.websocket.Session;

import tk.risingnations.server.net.protocol.GameProtocolRequest;
import tk.risingnations.server.support.beans.Bean;

@Bean
public interface IBroadcastService {
	void addSession(Session session);

	void removeSession(Session session);

	void broadcast(GameProtocolRequest request);
}
