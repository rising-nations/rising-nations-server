package tk.risingnations.server.net.protocol;

import java.io.Serializable;

public class GameProtocolRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;
	private String action;
	private String data;

	public String getAction() {
		return action;
	}

	public String getId() {
		return id;
	}

	public String getData() {
		return data;
	}
}
