package tk.risingnations.server.net.protocol;

import java.io.Serializable;

public class GameProtocolResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;
	private boolean successful;
	private Object data;

	public GameProtocolResponse(String id, Object data) {
		this.id = id;
		this.data = data;
		this.successful = true;
	}

	public GameProtocolResponse(String id, boolean successful) {
		this.id = id;
		this.successful = successful;
	}

	public GameProtocolResponse(String id, boolean successful, Object errorData) {
		this.id = id;
		this.successful = successful;
		this.data = errorData;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public String getId() {
		return id;
	}

	public Object getData() {
		return data;
	}
}
