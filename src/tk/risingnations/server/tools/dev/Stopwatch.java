package tk.risingnations.server.tools.dev;

import java.util.ArrayList;
import java.util.List;

import tk.risingnations.server.support.logging.LOG;

public class Stopwatch {
	private static List<Long> startDates = new ArrayList<Long>();

	public static void start() {
		if (LOG.isFinerEnabled()) {
			startDates.add(System.nanoTime());
		}
	}

	public static void stop(String name) {
		if (LOG.isFinerEnabled()) {
			long endTime = System.nanoTime();
			long duration = endTime - startDates.remove(startDates.size() - 1);
			double durationMs = duration / 1000000;
			LOG.finer(name, " took ", durationMs + "", "ms");
		}
	}
}
