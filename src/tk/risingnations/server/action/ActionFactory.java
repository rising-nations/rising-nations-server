/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.action;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import tk.risingnations.server.action.common.GameAction;
import tk.risingnations.server.action.common.IGameAction;
import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.support.beans.BEANS;
import tk.risingnations.server.support.classinventory.IClassInventoryService;

public class ActionFactory implements IActionFactory {
	private final Map<String, Class<?>> m_actionRegistry = Collections.synchronizedMap(new HashMap<>());
	private final Map<Class<?>, IGameAction> m_actionInstanceRegistry = Collections.synchronizedMap(new HashMap<>());
	private volatile boolean initialized;

	public ActionFactory() {
	}

	public void ensureInitialized() {
		if (!initialized) {
			List<Class<?>> gameActions = BEANS.get(IClassInventoryService.class).getClassesWithAnnotation(GameAction.class);

			for (Class<?> gameAction : gameActions) {
				GameAction annotation = gameAction.getAnnotation(GameAction.class);
				m_actionRegistry.put(annotation.value(), gameAction);
			}
		}
	}

	@Override
	public IGameAction get(String action) {
		ensureInitialized();
		if (action == null || StringUtils.isEmpty(action)) {
			throw new ProcessingException("action cannot be null");
		}
		action = action.trim().toLowerCase();
		Class<?> actionClazz = m_actionRegistry.get(action);
		if (actionClazz == null) {
			throw new ProcessingException("action '" + action + "' not found");
		}
		synchronized (m_actionInstanceRegistry) { // ensure only one instance is created
			if (m_actionInstanceRegistry.containsKey(action)) {
				return m_actionInstanceRegistry.get(action);
			}

			try {

				IGameAction actionInst = (IGameAction) actionClazz.newInstance();
				m_actionInstanceRegistry.put(actionClazz, actionInst);
				return actionInst;

			} catch (InstantiationException | IllegalAccessException e) {
				throw new ProcessingException("failed to instantiate class " + actionClazz.getName());
			}
		}
	}
}
