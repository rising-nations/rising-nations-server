/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.action;

import tk.risingnations.server.action.common.IGameAction;
import tk.risingnations.server.support.beans.Bean;

@Bean
public interface IActionFactory {

	IGameAction get(String action);

}
