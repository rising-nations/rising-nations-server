/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.action.common;

/**
 * Always attach actions with the {@link GameAction}
 * 
 * @author skeeks
 *
 * @see GameAction
 */
public interface IGameAction {
}
