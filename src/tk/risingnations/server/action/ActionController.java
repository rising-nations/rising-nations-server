/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.action;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import tk.risingnations.server.action.common.GameHandler;
import tk.risingnations.server.action.common.IGameAction;
import tk.risingnations.server.action.common.IRequest;
import tk.risingnations.server.errorhandling.GameProcessingException;
import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.net.protocol.GameProtocolRequest;
import tk.risingnations.server.net.protocol.GameProtocolResponse;
import tk.risingnations.server.support.beans.BEANS;
import tk.risingnations.server.support.classinventory.IClassInventoryService;
import tk.risingnations.server.support.json.IJsonService;

public class ActionController implements IActionController {
	private final Map<IGameAction, Method> m_methodCache = Collections.synchronizedMap(new HashMap<>());

	public ActionController() {
	}

	@Override
	public String handle(tk.risingnations.server.net.protocol.GameProtocolRequest request) {
		IGameAction action = BEANS.get(IActionFactory.class).get(request.getAction());
		Method handleMethod = getHandleMethod(action);

		Object responseData = null;
		if (handleMethod.getParameterCount() == 0) {
			responseData = executeGameHandler(request, action, handleMethod);
		} else {
			responseData = executeGameHandlerWithRequest(request, action, handleMethod);
		}

		GameProtocolResponse response = new GameProtocolResponse(request.getId(), responseData);
		return BEANS.get(IJsonService.class).writeToString(response);
	}

	private Object executeGameHandler(GameProtocolRequest request, IGameAction action, Method handleMethod) {
		try {
			return handleMethod.invoke(action);
		} catch (ProcessingException e) {
			throw new GameProcessingException("error executing game action handler", e);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new ProcessingException("failed to execute '" + action.getClass().getName() + "." + handleMethod.getName() + "'", e);
		}
	}

	private Object executeGameHandlerWithRequest(GameProtocolRequest request, IGameAction action, Method handleMethod) {
		Object requestData = null;
		try {
			requestData = BEANS.get(IJsonService.class).readFromString(request.getData(), handleMethod.getParameters()[0].getType());
		} catch (ProcessingException e) {
			throw new ProcessingException("failed to parse json into method type", e);
		}
		try {
			return handleMethod.invoke(action, requestData);
		} catch (GameProcessingException e) {
			throw new ProcessingException("error executing game action handler", e);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new ProcessingException("failed to execute '" + action.getClass().getName() + "." + handleMethod.getName() + "'", e);
		}
	}

	/**
	 * Searches in the GameAction for a Method with the annotation GameHandler. The Methods are stored in a map for performance reasons. It also checks the method, if its a valid GameHandler method
	 * 
	 * Fails if:
	 * - no method with {@link GameHandler} is found
	 * - the method is invalid for a {@link GameHandler} method
	 * 
	 * @see GameHandler
	 * @see IGameAction
	 */
	private Method getHandleMethod(IGameAction action) {
		if (m_methodCache.containsKey(action)) {
			return m_methodCache.get(action);
		} else {
			Method method = BEANS.get(IClassInventoryService.class).getMethodWithAnnotation(action.getClass(), GameHandler.class);
			if (method == null) {
				throw new RuntimeException("'" + action.getClass().getName() + "' has no method with " + GameHandler.class.getName() + " annotation");
			}
			checkForMethodValidity(method);
			synchronized (m_methodCache) {
				if (m_methodCache.containsKey(action)) { // check again
					return m_methodCache.get(action);
				}
				m_methodCache.put(action, method);
				return method;
			}
		}
	}

	/**
	 * Method has to:
	 * - 0 or 1 parameter
	 * - if 1 parameter: type must extends IRequest
	 */
	private void checkForMethodValidity(Method method) {
		if (method.getParameterCount() > 1 ||
				(method.getParameterCount() != 0 && method.getParameters()[0].getType().isAssignableFrom(IRequest.class))) {
			throw new RuntimeException("method '" + method.getDeclaringClass().getClass().getName() + "." + method.getName() + "' is no valid '" + GameHandler.class.getName() + "' method");
		}
	}
}
