/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.auth;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import tk.risingnations.server.account.AccountEntity;
import tk.risingnations.server.auth.common.AuthConstants;
import tk.risingnations.server.auth.common.AuthTexts;
import tk.risingnations.server.auth.common.ResponseUtility;
import tk.risingnations.server.auth.jwt.IJwtService;
import tk.risingnations.server.auth.request.LoginRequest;
import tk.risingnations.server.auth.responses.AuthTokenResponse;
import tk.risingnations.server.auth.responses.FailResponse;
import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.support.beans.BEANS;
import tk.risingnations.server.support.hashing.IHashService;
import tk.risingnations.server.support.json.IJsonService;

/**
 *
 * @author skeeks
 */
@WebServlet(name = "LoginServlet", urlPatterns = { "/auth/login" })
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			LoginRequest data = BEANS.get(IJsonService.class).read(req.getInputStream(), LoginRequest.class);

			boolean valid = true;
			valid = valid && !StringUtils.isEmpty(data.username);
			valid = valid && !StringUtils.isEmpty(data.password);

			valid = valid && StringUtils.length(data.username) > AuthConstants.MIN_USERNAME_LENGTH;
			valid = valid && StringUtils.length(data.password) >= AuthConstants.MIN_PASSWORD_LENGTH;

			if (!valid) {
				ResponseUtility.badRequest(resp, new FailResponse(AuthTexts.get("UsernameAndPasswordRequired", req.getLocale().getLanguage())));
				return;
			}
			data.username = StringUtils.lowerCase(data.username);

			AccountEntity account = AccountEntity.loadByUsername(data.username);
			if (account == null) {
				ResponseUtility.badRequest(resp, new FailResponse(AuthTexts.get("InvalidLoginData", req.getLocale().getLanguage())));
				return;
			}

			// Check received password against the password from the db.
			boolean authenticated = BEANS.get(IHashService.class).check(data.password, account.getPassword());

			// key place. if user entered false password, return
			if (!authenticated) {
				ResponseUtility.badRequest(resp, new FailResponse(AuthTexts.get("InvalidLoginData", req.getLocale().getLanguage())));
				return;
			}

			// Log in
			String authToken = BEANS.get(IJwtService.class).build(account.getUsername());

			ResponseUtility.success(resp, new AuthTokenResponse(authToken));

		} catch (ProcessingException e) {
			ResponseUtility.badRequest(resp, new FailResponse("wrong json format"));
		}

	}
}
