package tk.risingnations.server.auth;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import tk.risingnations.server.account.AccountEntity;
import tk.risingnations.server.auth.common.ResponseUtility;
import tk.risingnations.server.auth.jwt.IJwtService;
import tk.risingnations.server.auth.request.AuthTokenRequest;
import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.support.beans.BEANS;
import tk.risingnations.server.support.json.IJsonService;

@WebServlet(description = "CheckAuthStateServlet", urlPatterns = { "/auth/check" })
public class CheckAuthStateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			AuthTokenRequest request = BEANS.get(IJsonService.class).read(req.getInputStream(), AuthTokenRequest.class);

			if (StringUtils.isEmpty(request.getAuthToken())) {
				ResponseUtility.authRequired(resp, null);
			}

			String username = BEANS.get(IJwtService.class).parseUsername(request.getAuthToken());

			AccountEntity account = AccountEntity.loadByUsername(username);
			if (account == null) {
				throw new ProcessingException("no account found with username '" + username + "'");
			}
			ResponseUtility.success(resp, null);

		} catch (Exception e) {
			ResponseUtility.authRequired(resp, null);
		}
	}
}
