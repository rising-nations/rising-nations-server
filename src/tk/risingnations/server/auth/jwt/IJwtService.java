package tk.risingnations.server.auth.jwt;

import tk.risingnations.server.support.beans.Bean;

@Bean
public interface IJwtService {
	public String parseUsername(String jwt);

	public String build(String username);
}
