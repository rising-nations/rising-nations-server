package tk.risingnations.server.auth.jwt;

import java.security.KeyPair;

import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.lang.JoseException;

import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.support.config.CONFIG;
import tk.risingnations.server.support.logging.LOG;

public class JwtService implements IJwtService {
	public static final String JWT_EXPIRATION_TIME_PROPERTY = "jwt.expiration_time";
	public static final String JWT_KEYPAIR_PROPERTY = "jwt.json_webkey";
	private KeyPair m_cachedKeyPair;

	public KeyPair getKeyPair() {
		if (m_cachedKeyPair == null) {
			try {
				RsaJsonWebKey rsaKeyPair = (RsaJsonWebKey) RsaJsonWebKey.Factory.newPublicJwk(CONFIG.get(JWT_KEYPAIR_PROPERTY));
				m_cachedKeyPair = new KeyPair(rsaKeyPair.getPublicKey(), rsaKeyPair.getPrivateKey());
			} catch (JoseException e) {
				LOG.severe(e, "failed to read json web key");
				throw new ProcessingException("failed to read json web key", e);
			}
		}
		return m_cachedKeyPair;
	}

	@Override
	public String parseUsername(String jwt) {
		try {
			JwtConsumer consumer = new JwtConsumerBuilder()
					.setRequireExpirationTime()
					.setRequireSubject()
					.setAllowedClockSkewInSeconds(30)
					.setExpectedIssuer("Rising Nations")
					.setVerificationKey(getKeyPair().getPublic()) // public key
					.build();

			JwtClaims claims = consumer.processToClaims(jwt);
			return claims.getSubject();
		} catch (InvalidJwtException | MalformedClaimException e) {
			throw new ProcessingException("jwt could not be consumed", e);
		}

	}

	@Override
	public String build(String username) {
		try {
			JwtClaims claims = new JwtClaims();
			claims.setIssuer("Rising Nations"); // who creates the token and signs it
			claims.setExpirationTimeMinutesInTheFuture(CONFIG.getInt(JWT_EXPIRATION_TIME_PROPERTY)); // time when the token will expire (10 minutes from now)
			claims.setGeneratedJwtId(); // a unique identifier for the token
			claims.setIssuedAtToNow(); // when the token was issued/created (now)
			claims.setNotBeforeMinutesInThePast(2); // time before which the token is not yet valid (2 minutes ago)
			claims.setSubject(username); // the subject/principal is whom the token is about

			JsonWebSignature jws = new JsonWebSignature();
			jws.setPayload(claims.toJson());
			jws.setKey(getKeyPair().getPrivate());
			jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

			return jws.getCompactSerialization();
		} catch (JoseException e) {
			throw new ProcessingException("failed to build jwt", e);
		}
	}

}
