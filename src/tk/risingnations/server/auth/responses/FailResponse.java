package tk.risingnations.server.auth.responses;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FailResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	public List<String> errors = new ArrayList<String>();

	public FailResponse(String... errors) {
		this.errors.addAll(Arrays.asList(errors));
	}

	public void addError(String error) {
		errors.add(error);
	}

	public List<String> getErrors() {
		return errors;
	}
}
