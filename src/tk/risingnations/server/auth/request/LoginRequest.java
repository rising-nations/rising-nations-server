package tk.risingnations.server.auth.request;

import java.io.Serializable;

public class LoginRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	public String username;
	public String password;
}
