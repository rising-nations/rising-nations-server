package tk.risingnations.server.auth.request;

import java.io.Serializable;

public class RegistrationRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	public String username;
	public String email;
	public String password;
	public boolean newsletterSubscribed;
	public boolean agbAccepted;

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return username;
	}

	public boolean isNewsletterSubscribed() {
		return newsletterSubscribed;
	}

	public boolean isAgbAccepted() {
		return agbAccepted;
	}

	public void setAgbAccepted(boolean agbAccepted) {
		this.agbAccepted = agbAccepted;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setNewsletterSubscribed(boolean newsletterSubscribed) {
		this.newsletterSubscribed = newsletterSubscribed;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
