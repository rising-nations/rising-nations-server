package tk.risingnations.server.auth.common;

import java.util.HashMap;
import java.util.Map;

public class AuthTexts {
	private static Map<String, Map<String, String>> texts;

	static {
		texts = new HashMap<String, Map<String, String>>();
		texts.put("en", new HashMap<String, String>());
		texts.put("de", new HashMap<String, String>());
		
		texts.get("en").put("UsernameAndPasswordRequired", "You need to enter a username and a password");
		texts.get("de").put("UsernameAndPasswordRequired", "Du musst einen Benutzernamen und ein Passwort eingeben");
		
		texts.get("en").put("InvalidLoginData", "Username and/or Password wrong");
		texts.get("de").put("InvalidLoginData", "Benutzername und/oder Passwort sind falsch");

		texts.get("en").put("InvalidUsername", "Your Username needs to be longer than 3 characters");
		texts.get("de").put("InvalidUsername", "Der Benutzername muss l�nger als 3 Zeichen sein");
		
		texts.get("en").put("InvalidPassword", "Please enter a password with a least 8 characters");
		texts.get("de").put("InvalidPassword", "Bitte gib ein Passwort mit mindestens 8 Zeichen ein");
		
		texts.get("en").put("InvalidEmail", "Please enter a valid e-mail address");
		texts.get("de").put("InvalidEmail", "Bitte gib eine g�ltige E-Mail Adresse an");
		
		texts.get("en").put("InvalidAGB", "Please accept the Terms and Conditions");
		texts.get("de").put("InvalidAGB", "Bitte akzeptiere die AGB");
		
		texts.get("en").put("NonUniqueUsername", "Sorry! but your username is already taken");
		texts.get("de").put("NonUniqueUsername", "Sorry! Der Benutzername ist bereits vergeben");
	}

	public static String get(String keyword, String locale) {
		if (!texts.containsKey(locale)) {
			return get(keyword, "en");
		}
		Map<String, String> keywordMap = texts.get(locale);
		return keywordMap.get(keyword);
	}
}
