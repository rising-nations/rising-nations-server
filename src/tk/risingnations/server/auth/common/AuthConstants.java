package tk.risingnations.server.auth.common;

import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.lang.JoseException;

import tk.risingnations.server.support.logging.LOG;

public class AuthConstants {
	private AuthConstants() {

	}

	public static final int MIN_USERNAME_LENGTH = 3;
	public static final int MIN_PASSWORD_LENGTH = 8;

	public final static String PROP_SUBJECT = "subject";
	public final static String WORLD_ATTR_NAME = "world";
	public static RsaJsonWebKey RSA_WEB_KEY;
	{
		try {
			RSA_WEB_KEY = RsaJwkGenerator.generateJwk(2048);
		} catch (JoseException e) {
			LOG.severe("Failed to generate new rsa key pair. Authorization will not work!!");
		}
	}
}
