package tk.risingnations.server.auth.common;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import tk.risingnations.server.support.beans.BEANS;
import tk.risingnations.server.support.json.IJsonService;

public class ResponseUtility {
	private ResponseUtility() {

	}

	/**
	 * send 200 success response
	 * 
	 */
	public static void success(HttpServletResponse resp, Object jsonData) throws IOException {
		resp.setStatus(HttpServletResponse.SC_OK);
		if (jsonData != null) {
			resp.setContentType("application/json");
			BEANS.get(IJsonService.class).write(jsonData, resp.getOutputStream());
		}
		resp.flushBuffer();
	}

	/**
	 * Sends a 501 error
	 */
	public static void authRequired(HttpServletResponse resp, Object jsonData) throws IOException {
		resp.setStatus(HttpServletResponse.SC_PROXY_AUTHENTICATION_REQUIRED);
		if (jsonData != null) {
			resp.setContentType("application/json");
			BEANS.get(IJsonService.class).write(jsonData, resp.getOutputStream());
		}
		resp.flushBuffer();
	}

	/**
	 * send bad request error
	 */
	public static void badRequest(HttpServletResponse resp, Object jsonData) throws IOException {
		resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		if (jsonData != null) {
			resp.setContentType("application/json");
			BEANS.get(IJsonService.class).write(jsonData, resp.getOutputStream());
		}
		resp.flushBuffer();
	}
}
