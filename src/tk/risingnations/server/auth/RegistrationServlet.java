package tk.risingnations.server.auth;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import tk.risingnations.server.account.AccountEntity;
import tk.risingnations.server.auth.common.AuthConstants;
import tk.risingnations.server.auth.common.AuthTexts;
import tk.risingnations.server.auth.common.ResponseUtility;
import tk.risingnations.server.auth.jwt.IJwtService;
import tk.risingnations.server.auth.request.RegistrationRequest;
import tk.risingnations.server.auth.responses.AuthTokenResponse;
import tk.risingnations.server.auth.responses.FailResponse;
import tk.risingnations.server.database.IMongoDbService;
import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.support.beans.BEANS;
import tk.risingnations.server.support.hashing.IHashService;
import tk.risingnations.server.support.json.IJsonService;

//TODO [ske] needs a new mechanism for JWT
@WebServlet(name = "RegistrationServlet", urlPatterns = { "/auth/registration" })
public class RegistrationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			RegistrationRequest input = BEANS.get(IJsonService.class).read(req.getInputStream(), RegistrationRequest.class);
			// TODO [ske] data has to get escape
			FailResponse output = new FailResponse();
			if (!this.validate(input, output, req.getLocale())) {
				ResponseUtility.badRequest(resp, output);
				return;
			}

			AccountEntity existingAccount = AccountEntity.loadByUsername(input.getUsername());
			if (existingAccount != null) {
				output.addError(AuthTexts.get("NonUniqueUsername", req.getLocale().getLanguage()));
				ResponseUtility.badRequest(resp, output);
				return;
			}
			existingAccount = null;

			input.setUsername(StringUtils.lowerCase(input.getUsername()));

			AccountEntity account = new AccountEntity();
			account.setUsername(input.getUsername());
			account.setPassword(BEANS.get(IHashService.class).hash(input.getPassword()));
			account.setEmail(input.getEmail());
			account.setNewsletterSubscribed(input.isNewsletterSubscribed());

			BEANS.get(IMongoDbService.class).create(account);

			String jwtToken = BEANS.get(IJwtService.class).build(account.getUsername());
			ResponseUtility.success(resp, new AuthTokenResponse(jwtToken));

		} catch (ProcessingException e) {
			ResponseUtility.badRequest(resp, new FailResponse("json format error"));
		}
	}

	protected boolean validate(RegistrationRequest input, FailResponse output, Locale locale) {

		if (StringUtils.isEmpty(input.getUsername()) && StringUtils.length(input.getUsername()) < AuthConstants.MIN_USERNAME_LENGTH) {
			output.addError(AuthTexts.get("InvalidUsername", locale.getLanguage()));
		}
		if (StringUtils.isEmpty(input.getPassword()) || StringUtils.length(input.getPassword()) < AuthConstants.MIN_PASSWORD_LENGTH) {
			output.addError(AuthTexts.get("InvalidPassword", locale.getLanguage()));
		}

		// TODO [skeeks] email validation
		if (StringUtils.isEmpty(input.getEmail()) || StringUtils.length(input.getEmail()) <= 3) {
			output.addError(AuthTexts.get("InvalidEmail", locale.getLanguage()));
		}
		if (!input.isAgbAccepted()) {
			output.addError(AuthTexts.get("InvalidAGB", locale.getLanguage()));
		}
		return output.getErrors().isEmpty();
	}
}
