package tk.risingnations.server.startup;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import tk.risingnations.server.support.beans.BeansInjector;
import tk.risingnations.server.support.config.CONFIG;
import tk.risingnations.server.support.logging.LOG;

@WebListener
public class RisingNationsStartupListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent event) {
		try {
			LOG.init();
			BeansInjector.start();
			LOG.info("Rising Nations ", CONFIG.get("version"), " started!");
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("FATAL ERROR ON INITALIZING RISINGNATIONS. RESTART REQUIRED");
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {

	}
}
