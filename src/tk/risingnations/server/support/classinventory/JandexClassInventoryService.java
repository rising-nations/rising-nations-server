package tk.risingnations.server.support.classinventory;

import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.jboss.jandex.AnnotationInstance;
import org.jboss.jandex.ClassInfo;
import org.jboss.jandex.DotName;
import org.jboss.jandex.Index;
import org.jboss.jandex.IndexReader;

import tk.risingnations.server.support.logging.LOG;

public class JandexClassInventoryService implements IClassInventoryService {
	private Index m_index;

	public JandexClassInventoryService() {
		loadIndex();
	}

	private void loadIndex() {
		try (InputStream input = JandexClassInventoryService.class.getClassLoader().getResourceAsStream("../index.idx")) {
			IndexReader reader = new IndexReader(input);
			m_index = reader.read();
		} catch (Exception e) {
			e.printStackTrace();
			LOG.severe("FAILED TO READ CLASS INVENTORY");
		}
	}

	@Override
	public List<Class<?>> getClassesWithAnnotation(Class<?> annotationClazz) {
		DotName annotationName = DotName.createSimple(annotationClazz.getName());
		List<AnnotationInstance> annotations = getIndex().getAnnotations(annotationName);
		List<Class<?>> clazzes = new ArrayList<Class<?>>(annotations.size());
		for (AnnotationInstance instance : annotations) {
			try {
				clazzes.add(Class.forName(instance.target().asClass().name().toString()));
			} catch (ClassNotFoundException e) {
				LOG.severe("Class for name '" + instance.target().asClass().name().toString() + "' not found! Is the index invalid?");
			}
		}
		return clazzes;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<Class<T>> getAllImplementors(Class<T> bean) {
		DotName interfaceName = DotName.createSimple(bean.getName());
		Set<ClassInfo> implementors = getIndex().getAllKnownImplementors(interfaceName);
		List<Class<T>> concreteImplementors = new ArrayList<>(implementors.size());
		for (ClassInfo clazz : implementors) {
			if (!Modifier.isAbstract(clazz.flags())) {
				try {
					concreteImplementors.add((Class<T>) Class.forName(clazz.name().toString()));
				} catch (ClassNotFoundException e) {
					LOG.severe("Failed to create class for name '" + clazz.name().toString() + "'");
				}
			}
		}
		return concreteImplementors;
	}

	@Override
	public <T extends Annotation> Method getMethodWithAnnotation(Class<?> clazz, Class<T> annotationClazz) {
		for (Method method : clazz.getMethods()) {
			if (method.isAnnotationPresent(annotationClazz)) {
				return method;
			}
		}
		return null;
	}

	public Index getIndex() {
		return m_index;
	}
}
