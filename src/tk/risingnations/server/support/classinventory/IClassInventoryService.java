package tk.risingnations.server.support.classinventory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;

public interface IClassInventoryService {

	List<Class<?>> getClassesWithAnnotation(Class<?> annotationClazz);

	<T> List<Class<T>> getAllImplementors(Class<T> bean);

	<T extends Annotation> Method getMethodWithAnnotation(Class<?> clazz, Class<T> annotationClazz);
}
