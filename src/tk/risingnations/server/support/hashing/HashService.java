package tk.risingnations.server.support.hashing;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.Random;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import tk.risingnations.server.support.logging.LOG;

public class HashService implements IHashService {
	private static final String DELIMITER = "$";
	private Random m_random;

	public HashService() {
		m_random = new Random();
	}

	public Random getRandom() {
		return m_random;
	}

	@Override
	public String hash(String phrase) {
		byte[] salt = new byte[16];
		getRandom().nextBytes(salt);

		String hashed = hash(phrase, salt);

		return hashed;
	}

	@Override
	public String hash(String phrase, byte[] salt) {
		if (phrase == null) {
			throw new IllegalArgumentException("phrase cannot be null");
		}
		if (salt == null) {
			throw new IllegalArgumentException("salt cannot be null");
		}
		try {
			KeySpec spec = new PBEKeySpec(phrase.toCharArray(), salt, 65536, 128);
			SecretKeyFactory f;
			f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash = f.generateSecret(spec).getEncoded();
			Encoder bs64 = Base64.getEncoder();
			return bs64.encodeToString(salt) + DELIMITER + bs64.encodeToString(hash);

		} catch (NoSuchAlgorithmException e) {
			LOG.severe("Could not hash string, algorithm PBKDF2WithHmacSHA1 not found", e.getMessage());
			throw new RuntimeException("ould not hash string, algorithm PBKDF2WithHmacSHA1 not found", e);
		} catch (InvalidKeySpecException e) {
			LOG.severe("Could not hash string, invalid key spec", e.getMessage());
			throw new RuntimeException("Could not hash string, invalid key spec", e);
		}
	}

	@Override
	public boolean check(String plain, String hashed) {
		if (hashed == null) {
			throw new IllegalArgumentException("hashed cannot be null");
		}
		if (plain == null) {
			throw new IllegalArgumentException("plain cannot be null");
		}
		// we need to escape $ because its a special regex char
		String[] splitted = hashed.split("\\$");
		if (splitted.length != 2) {
			throw new IllegalArgumentException("arguments hashed needs to be a string of structure <salt>" + DELIMITER + "<hash>");
		}
		Decoder decoder = Base64.getDecoder();

		byte[] salt = decoder.decode(splitted[0]);

		String newlyHashed = hash(plain, salt);

		return newlyHashed.equals(hashed);

	}
}
