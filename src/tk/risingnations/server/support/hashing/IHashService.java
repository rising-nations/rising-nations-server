package tk.risingnations.server.support.hashing;

import tk.risingnations.server.support.beans.Bean;

@Bean
public interface IHashService {

	String hash(String phrase);

	String hash(String phrase, byte[] salt);

	boolean check(String plain, String hashed);

}
