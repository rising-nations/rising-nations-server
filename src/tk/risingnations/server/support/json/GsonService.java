package tk.risingnations.server.support.json;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

import com.google.gson.Gson;

import tk.risingnations.server.errorhandling.ProcessingException;

public class GsonService implements IJsonService {

	@Override
	public void write(Object pojo, OutputStream out) throws ProcessingException {
		Gson gson = new Gson();
		try (OutputStreamWriter writer = new OutputStreamWriter(out)) {
			writer.write(gson.toJson(pojo));
		} catch (IOException e) {
			throw new ProcessingException(e);
		}
	}

	@Override
	public void write(Object pojo, Writer writer) throws ProcessingException {
		Gson gson = new Gson();
		try {
			writer.write(gson.toJson(pojo));
		} catch (IOException e) {
			throw new ProcessingException(e);
		}
	}

	@Override
	public String writeToString(Object pojo) throws ProcessingException {
		return new Gson().toJson(pojo);
	}

	@Override
	public <T> T read(InputStream in, Class<T> clazz) throws ProcessingException {
		try (Reader reader = new InputStreamReader(in)) {
			Gson gson = new Gson();
			return gson.fromJson(reader, clazz);
		} catch (IOException e) {
			throw new ProcessingException(e);
		}
	}

	@Override
	public <T> T readFromString(String json, Class<T> clazz) throws ProcessingException {
		return new Gson().fromJson(json, clazz);
	}

}
