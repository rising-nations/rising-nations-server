/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.support.json;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;

import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.support.beans.Bean;

@Bean
public interface IJsonService {

	void write(Object pojo, OutputStream out) throws ProcessingException;

	void write(Object pojo, Writer out) throws ProcessingException;

	String writeToString(Object pojo) throws ProcessingException;

	<T> T read(InputStream in, Class<T> clazz) throws ProcessingException;

	<T> T readFromString(String json, Class<T> clazz) throws ProcessingException;
}
