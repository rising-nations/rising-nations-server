package tk.risingnations.server.support.config;

import tk.risingnations.server.support.beans.Bean;

@Bean
public interface IConfigService {

	String get(String key);

	int getInt(String key);

	/**
	 * Sets the config, but this option is not persistes! It only exists for the
	 * runtime.
	 * 
	 * @param key
	 */
	void set(String key, String value);

	double getDouble(String key);
}
