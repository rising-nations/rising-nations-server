package tk.risingnations.server.support.config;

import java.io.InputStream;
import java.util.Map;

import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.support.beans.BEANS;
import tk.risingnations.server.support.json.IJsonService;
import tk.risingnations.server.support.logging.LOG;

public class ConfigService implements IConfigService {
	private Map<String, Object> configMap;

	public ConfigService() {
		load();
	}

	@SuppressWarnings("unchecked")
	public void load() {
		try {
			InputStream in = ConfigService.class.getClassLoader().getResourceAsStream("../config.json");
			configMap = BEANS.get(IJsonService.class).read(in, Map.class);
		} catch (ProcessingException e) {
			LOG.severe("Could not load config file");
		}
	}

	@Override
	public String get(String key) {
		Object value = configMap.get(key);
		if (value != null) {
			return value.toString();
		} else {
			return null;
		}
	}

	@Override
	public double getDouble(String key) {
		String val = get(key);
		try {
			return Double.valueOf(val);
		} catch (NumberFormatException e) {
			throw new ProcessingException("config property '" + key + "' is not a number!");
		}
	}

	@Override
	public int getInt(String key) {
		double val = getDouble(key);

		return Double.valueOf(val).intValue();
	}

	@Override
	public void set(String key, String value) {
		configMap.put(key, value);
	}
}
