package tk.risingnations.server.support.config;

import tk.risingnations.server.support.beans.BEANS;

public class CONFIG {
	private static final IConfigService SERVICE = BEANS.get(IConfigService.class);

	public static String get(String key) {
		return SERVICE.get(key);
	}

	public static double getDouble(String key) {
		return SERVICE.getDouble(key);
	}

	public static int getInt(String key) {
		return SERVICE.getInt(key);
	}

	public static void set(String key, String value) {
		SERVICE.set(key, value);
	}

}
