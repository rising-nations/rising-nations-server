/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.support.beans;

import java.util.List;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;

import tk.risingnations.server.support.classinventory.IClassInventoryService;
import tk.risingnations.server.support.classinventory.JandexClassInventoryService;
import tk.risingnations.server.support.config.ConfigService;
import tk.risingnations.server.support.config.IConfigService;
import tk.risingnations.server.support.logging.LOG;

/**
 * This google guice module is the single response point to get the instance of
 * a service.
 * 
 * @author skeeks
 */
public class BeansInjector extends AbstractModule {
	private static Injector INJECTOR_INSTANCE;

	protected static Injector getInjectorInstance() {
		if (INJECTOR_INSTANCE == null) {
			throw new RuntimeException("BeansInjector is not ready yet");
		}
		return INJECTOR_INSTANCE;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected void configure() {
		// we need the class inventory service for searching all beans
		IClassInventoryService inventorySvc = new JandexClassInventoryService();
		bind(IClassInventoryService.class).toInstance(inventorySvc);
		bind(IConfigService.class).to(ConfigService.class).in(Singleton.class);

		List<Class<?>> beans = inventorySvc.getClassesWithAnnotation(Bean.class);
		for (Class bean : beans) {
			Bean beanAnnotation = (Bean) bean.getAnnotation(Bean.class);
			List<Class<?>> implementors = inventorySvc.getAllImplementors(bean);
			if (implementors.size() == 0) {
				LOG.warning("no implementation for '" + bean.getName() + "' found");
				continue;
			}
			Class beanImpl = (Class) implementors.get(0);
			if (implementors.size() > 1) {
				LOG.warning("More than 1 implementor found for '" + bean.getName() + "'");
			}
			try {
				if (beanAnnotation.singleton()) {
					bind(bean).to(beanImpl).in(Singleton.class);
					LOG.finer("Registered '" + beanImpl.getName() + "' for '" + bean.getName() + "' as Singleton");
				} else {
					bind(bean).to(beanImpl); // new instance on each time BEANS.get is called
					LOG.finer("Registered '" + beanImpl.getName() + "' for '" + bean.getName() + "'");
				}
			} catch (Exception e) {
				LOG.finer("Failed to register '" + beanImpl.getName() + "' for '" + bean.getName() + "'");
			}
		}
	}

	public static <T extends Object> T getInstance(Class<T> clazz) {
		return getInjectorInstance().getInstance(clazz);
	}

	public static void start() {
		INJECTOR_INSTANCE = Guice.createInjector(new BeansInjector());
		LOG.finer(BeansInjector.class.getSimpleName(), " initialized!");
	}
}
