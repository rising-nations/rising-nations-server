/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.support.beans;

/**
 * Shorthand class for ServiceInjector
 * @see BeansInjector
 * @author skeeks
 */
public class BEANS {
    public static <T extends Object> T get(Class<T> clazz){
        return BeansInjector.getInstance(clazz);
    }
}
