package tk.risingnations.server.support.logging;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class LOG {
	private static Logger LOGGER = Logger.getGlobal();

	public static void init() {
		Level level = Level.parse("FINER");
		LOGGER.setLevel(level);
		LOGGER.setUseParentHandlers(false);
		ConsoleHandler consoleHandler = new ConsoleHandler();
		consoleHandler.setLevel(level);
		for (Handler handler : LOGGER.getHandlers()) {
			LOGGER.removeHandler(handler);
		}
		LOGGER.addHandler(consoleHandler);
		LOGGER.getHandlers()[0].setFormatter(new CustomLogFormatter());
	}

	public static void setLevel(String level) {
		setLevel(Level.parse(level));
	}

	public static void setLevel(Level level) {
		LOGGER.setLevel(level);
	}

	public static void finer(String... messages) {
		log(Level.FINER, null, messages);
	}

	public static boolean isFinerEnabled() {
		return LOGGER.getLevel().intValue() <= Level.FINER.intValue();
	}

	public static void fine(String... messages) {
		log(Level.FINE, null, messages);
	}

	public static void fine(Throwable t, String... messages) {
		log(Level.FINE, t, messages);
	}

	public static void info(String... messages) {
		log(Level.INFO, null, messages);
	}

	public static void warning(String... messages) {
		log(Level.WARNING, null, messages);
	}

	public static void severe(String... messages) {
		log(Level.SEVERE, null, messages);
	}

	public static void severe(Throwable t, String... messages) {
		log(Level.SEVERE, t, messages);
	}

	protected static void log(Level level, Throwable t, String... messages) {
		// get class name of the class, which called the log method (fine, info,
		// severe, etc.)
		String callingClass = Thread.currentThread().getStackTrace()[3].getClassName();
		if (LOGGER.isLoggable(level)) {
			LOGGER.log(level, String.join("", messages) + " (" + callingClass + ")", t);
		}
	}

	public static class DefaultLogFormat extends Formatter {

		@Override
		public String format(LogRecord record) {
			Date date = new Date(record.getMillis());
			SimpleDateFormat df = new SimpleDateFormat("DD.MM.YYYY HH24:mm:ss");
			return "[" + df.format(date) + "] " + record.getMessage();
		}

	}

	public static class CustomLogFormatter extends Formatter {

		DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.YYYY hh:mm:ss");
		Date dat = new Date();

		public String format(LogRecord record) {

			// Minimize memory allocations here.
			dat.setTime(record.getMillis());

			StringBuilder text = new StringBuilder();
			// Date and time
			text.append('[');
			text.append(DATE_FORMAT.format(dat));
			// Level
			text.append(' ');
			text.append(record.getLevel().getName());
			text.append(']');
			text.append(' ');
			// Message
			text.append(record.getMessage());

			if (record.getThrown() != null) {
				try {
					StringWriter sw = new StringWriter();
					PrintWriter pw = new PrintWriter(sw);
					record.getThrown().printStackTrace(pw);
					pw.close();
					text.append(System.lineSeparator());
					text.append(sw.toString());
				} catch (Exception ex) {
				}
			}
			text.append(System.lineSeparator());
			return text.toString();
		}
	}
}
