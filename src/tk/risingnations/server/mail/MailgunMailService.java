package tk.risingnations.server.mail;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.support.config.CONFIG;

public class MailgunMailService implements IMailService {
	private static final String MAILGUN_API_URL = "https://api.mailgun.net/v3/@DOMAIN@/messages";
	private static final String AUTH_USER = "api";
	public static final String MAILGUN_API_KEY_PROPERTY = "mailgun.apikey";
	public static final String MAILGUN_DOMAIN_PROPERTY = "mailgun.domain";

	protected URL execCreateUrl() throws MalformedURLException {
		return new URL(MAILGUN_API_URL.replace("@DOMAIN@", CONFIG.get(MAILGUN_DOMAIN_PROPERTY)));
	}

	@Override
	public void send(String from, String to, String subject, String content) {
		try {
			URL url = null;
			url = execCreateUrl();
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			String userPassword = AUTH_USER + ":" + CONFIG.get(MAILGUN_API_KEY_PROPERTY);
			String encoding = Base64.getEncoder().encodeToString(userPassword.getBytes());

			StringBuilder query = new StringBuilder();
			query.append("from=" + URLEncoder.encode(from, "utf-8"));
			query.append("&to=" + URLEncoder.encode(to, "utf-8"));
			query.append("&subject=" + URLEncoder.encode(subject, "utf-8"));
			query.append("&html=" + URLEncoder.encode(content, "utf-8"));
			byte[] postData = query.toString().getBytes(StandardCharsets.UTF_8);

			conn.setDoOutput(true);
			conn.setInstanceFollowRedirects(false);
			conn.setRequestProperty("Authorization", "Basic " + encoding);
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("charset", "utf-8");
			conn.setRequestProperty("Content-Length", "" + postData.length);
			conn.setRequestMethod("POST");
			try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
				wr.write(postData);
			}

			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new ProcessingException(conn.getResponseMessage());
			}
			conn.disconnect();
		} catch (MalformedURLException e) {
			throw new ProcessingException("Malformed URL", e);
		} catch (Exception e) {
			throw new ProcessingException("error sending mail", e);
		}
	}
}
