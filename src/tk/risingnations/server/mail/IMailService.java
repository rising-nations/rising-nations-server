package tk.risingnations.server.mail;

import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.support.beans.Bean;

@Bean
public interface IMailService {
	/**
	 * Sends an email.
	 * 
	 * It is not guaranted, that the email is <b>really</b> sent, but if no
	 * exception is thrown
	 * 
	 * @param content
	 *            Content of the E-Mail. HTML is accepted
	 * @throws ProcessingException
	 *             if it fails to send the email.
	 */
	void send(String from, String to, String subject, String content) throws ProcessingException;
}
