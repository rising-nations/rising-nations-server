/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.errorhandling;

/**
 * Exception, which is thrown if business logic fails
 * 
 * @author skeeks
 */
public class GameProcessingException extends ProcessingException {

	private static final long serialVersionUID = 1L;

	public GameProcessingException() {

	}

	public GameProcessingException(String message) {
		super(message);
	}

	public GameProcessingException(String message, Throwable cause) {
		super(message, cause);
	}

	public GameProcessingException(Throwable cause) {
		super(cause);
	}
}
