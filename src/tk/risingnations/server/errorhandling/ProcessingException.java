/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.errorhandling;

/**
 *
 * @author skeeks
 */
public class ProcessingException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ProcessingException() {

	}

	public ProcessingException(String message) {
		super(message);
	}

	public ProcessingException(String message, Throwable cause) {
		super(message, cause);
	}

	public ProcessingException(Throwable cause) {
		super(cause);
	}
}
