package tk.risingnations.server.game.common;

import java.io.Serializable;

public class Coordinate implements Serializable {
	private static final long serialVersionUID = 1L;
	public int x;
	public int y;

	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Coordinate north() {
		return new Coordinate(this.x, this.y - 1);
	}

	public Coordinate northEast() {
		return new Coordinate(this.x + 1, this.y - 1);
	}

	public Coordinate east() {
		return new Coordinate(this.x + 1, this.y);
	}

	public Coordinate southEast() {
		return new Coordinate(this.x + 1, this.y + 1);
	}

	public Coordinate south() {
		return new Coordinate(this.x, this.y + 1);
	}

	public Coordinate southWest() {
		return new Coordinate(this.x - 1, this.y + 1);
	}

	public Coordinate west() {
		return new Coordinate(this.x - 1, this.y);
	}

	public Coordinate northWest() {
		return new Coordinate(this.x - 1, this.y - 1);
	}
}
