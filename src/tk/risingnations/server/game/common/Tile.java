/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.common;

import java.io.Serializable;

import tk.risingnations.server.game.territory.TerrainType;

public class Tile extends Coordinate implements Serializable {
	private static final long serialVersionUID = 1L;

	public TerrainType type;

	public Tile(int x, int y, TerrainType type) {
		super(x, y);
		this.type = type;
	}

	public Tile(Coordinate coordinate, TerrainType type) {
		super(coordinate.x, coordinate.y);
		this.type = type;
	}
}
