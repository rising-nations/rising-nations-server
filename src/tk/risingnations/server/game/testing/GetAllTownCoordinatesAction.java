package tk.risingnations.server.game.testing;

import java.util.ArrayList;
import java.util.List;

import tk.risingnations.server.action.common.GameHandler;
import tk.risingnations.server.action.common.IGameAction;
import tk.risingnations.server.action.common.IResponse;
import tk.risingnations.server.models.LocationModel;

@tk.risingnations.server.action.common.GameAction("all-town-coordinates")
public class GetAllTownCoordinatesAction implements IGameAction {

	@GameHandler
	public GetAllTownCoordinatesResponse exec() {
		GetAllTownCoordinatesResponse response = new GetAllTownCoordinatesResponse();
		response.towns.add(new LocationModel(10, 10));
		response.towns.add(new LocationModel(30, 30));
		response.towns.add(new LocationModel(54, 54));
		return response;
	}

	public static class GetAllTownCoordinatesResponse implements IResponse {
		private static final long serialVersionUID = 1L;
		public List<LocationModel> towns = new ArrayList<LocationModel>();
	}
}
