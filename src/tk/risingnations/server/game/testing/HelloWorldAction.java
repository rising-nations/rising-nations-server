package tk.risingnations.server.game.testing;

import tk.risingnations.server.action.common.GameAction;
import tk.risingnations.server.action.common.GameHandler;
import tk.risingnations.server.action.common.IGameAction;
import tk.risingnations.server.action.common.IResponse;

@GameAction("hello-world")
public class HelloWorldAction implements IGameAction {

	@GameHandler
	public HelloWorldResponse exec() {
		HelloWorldResponse response = new HelloWorldResponse();
		response.greeting = "Hello World [Action]";
		return response;
	}

	public static class HelloWorldResponse implements IResponse {
		private static final long serialVersionUID = 1L;

		public String greeting;
	}
}
