package tk.risingnations.server.game.testing;

import java.util.ArrayList;
import java.util.Date;

import tk.risingnations.server.action.common.GameAction;
import tk.risingnations.server.action.common.GameHandler;
import tk.risingnations.server.action.common.IGameAction;
import tk.risingnations.server.action.common.IResponse;
import tk.risingnations.server.game.building.BuildingType;
import tk.risingnations.server.game.map.LocationEntity;
import tk.risingnations.server.game.research.ResearchType;
import tk.risingnations.server.game.resources.ResourceType;
import tk.risingnations.server.models.BuildingUnderConstructionModel;
import tk.risingnations.server.models.ResourceModel;
import tk.risingnations.server.models.RunningResearchModel;
import tk.risingnations.server.models.TownModel;

@GameAction("town-overview")
public class TownOverviewAction implements IGameAction {

	@GameHandler
	public TownOverviewResponse exec() {
		TownOverviewResponse response = new TownOverviewResponse();
		response.name = "Black City";
		response.moral = 9;
		response.location = new LocationEntity(10, 10);

		// Researches
		response.researches = new ArrayList<>();
		RunningResearchModel research1 = new RunningResearchModel();
		research1.evtFinish = new Date(System.currentTimeMillis() + 100000);
		research1.toLevel = 2;
		research1.type = ResearchType.DRILLING_TECHNIQUES;
		response.researches.add(research1);

		// Buildings under construction
		response.buildingsUnderConstruction = new ArrayList<BuildingUnderConstructionModel>();
		BuildingUnderConstructionModel building1 = new BuildingUnderConstructionModel();
		building1.evtFinish = new Date(System.currentTimeMillis() + 100000);
		building1.toLevel = 5;
		building1.type = BuildingType.FARM;
		response.buildingsUnderConstruction.add(building1);

		response.resources = new ArrayList<>();
		ResourceModel resource1 = new ResourceModel();
		resource1.type = ResourceType.WHEAT;
		resource1.amount = 45000l;
		resource1.productionPerHour = 1000l;
		response.resources.add(resource1);
		return response;
	}

	public static class TownOverviewResponse extends TownModel implements IResponse {
		private static final long serialVersionUID = 1L;

	}
}
