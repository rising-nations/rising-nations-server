package tk.risingnations.server.game.territory;

public enum TerrainType {
	GRASSLAND("Grassland"), 
	MOUNTAINS("Mountains"), 
	FOREST("Forest"), 
	DESERT("Desert"), 
	TUNDRA("Tundra"), 
	SEA("Sea");

	private String m_textKeyword;

	TerrainType(String textKeyword) {
		this.m_textKeyword = textKeyword;
	}

	@Override
	public String toString() {
		return m_textKeyword;
	}
}
