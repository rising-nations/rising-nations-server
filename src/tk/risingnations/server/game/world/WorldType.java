package tk.risingnations.server.game.world;

public enum WorldType {
	WORLD_1("world1"), WORLD_2("world2");

	private String m_name;

	private WorldType(String name) {
		m_name = name;
	}

	public static WorldType getFromString(String possibleName) {
		if (possibleName == null) {
			return null;
		}
		for (WorldType type : WorldType.values()) {
			if (type.m_name.equalsIgnoreCase(possibleName)) {
				return type;
			}
		}
		return null;
	}
}
