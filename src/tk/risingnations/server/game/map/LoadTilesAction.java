/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.map;

import java.util.ArrayList;
import java.util.List;

import tk.risingnations.server.action.common.GameAction;
import tk.risingnations.server.action.common.GameHandler;
import tk.risingnations.server.action.common.IGameAction;
import tk.risingnations.server.game.common.Coordinate;
import tk.risingnations.server.game.common.Tile;
import tk.risingnations.server.game.territory.TerrainType;
import tk.risingnations.server.support.beans.BEANS;
import tk.risingnations.server.tools.dev.Stopwatch;

@GameAction("load-tiles")
public class LoadTilesAction implements IGameAction {

	@GameHandler
	public List<Tile> handle(Coordinate currentPosition) {
		Stopwatch.start();
		List<Coordinate> coordinates = new ArrayList<>();
		for (int x = -20; x < 20; x++) {
			for (int y = -20; y < 20; y++) {
				coordinates.add(new Coordinate(currentPosition.x + x, currentPosition.y + y));
			}
		}

		List<Tile> tiles = new ArrayList<>();
		IMapGenerationService svc = BEANS.get(IMapGenerationService.class);
		for (Coordinate coordinate : coordinates) {
			TerrainType type = svc.generateTerrainType(coordinate);
			tiles.add(new Tile(coordinate, type));
		}
		Stopwatch.stop("load-tiles");
		return tiles;
	}

}
