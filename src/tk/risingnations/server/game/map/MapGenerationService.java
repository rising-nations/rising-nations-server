package tk.risingnations.server.game.map;

import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.game.common.Coordinate;
import tk.risingnations.server.game.territory.TerrainType;
import tk.risingnations.server.support.config.CONFIG;

public class MapGenerationService implements IMapGenerationService {

	@Override
	public TerrainType generateTerrainType(LocationEntity location) {
		return generateTerrainType(location.getX(), location.getY());
	}

	@Override
	public TerrainType generateTerrainType(Coordinate coordinate) {
		return generateTerrainType(coordinate.x, coordinate.y);
	}

	@Override
	public TerrainType generateTerrainType(int x, int y) {
		checkBoundaries(x, y);
		double noiseValue = SimplexNoise.noise(x, y);
		return getTerrainTypeFromNoiseValue(noiseValue);
	}

	protected void checkBoundaries(int x, int y) {
		int maxX = CONFIG.getInt("map.max.x");
		int maxY = CONFIG.getInt("map.max.y");
		int minX = CONFIG.getInt("map.min.x");
		int minY = CONFIG.getInt("map.min.y");
		if (x > maxX || x < minX || y > maxY || y < minY) {
			throw new ProcessingException("coordinate is out of bondaries: Max: (" + maxX + "/" + maxY + ") / Min: (" + minX + "/" + minY + ")");
		}
	}

	protected TerrainType getTerrainTypeFromNoiseValue(double noiseValue) {
		if (noiseValue < 0.3) {
			return TerrainType.SEA;
		}
		if (noiseValue < 0.5) {
			return TerrainType.GRASSLAND;
		}
		if (noiseValue < 0.55) {
			return TerrainType.DESERT;
		}
		if (noiseValue < 0.7) {
			return TerrainType.TUNDRA;
		}
		if (noiseValue < 0.85) {
			return TerrainType.FOREST;
		}

		return TerrainType.MOUNTAINS;
	}
}
