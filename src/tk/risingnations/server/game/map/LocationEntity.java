/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.map;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

import tk.risingnations.server.database.IEntity;
import tk.risingnations.server.game.common.Coordinate;

@Entity(noClassnameStored = true)
public class LocationEntity implements IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	private ObjectId id;

	@Indexed
	private int x;

	@Indexed
	private int y;

	@Override
	public ObjectId getId() {
		return id;
	}

	@Override
	public void setId(ObjectId id) {
		this.id = id;
	}

	public LocationEntity(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (obj == this) {
			return true;
		}

		if (obj instanceof LocationEntity) {
			LocationEntity loc2 = (LocationEntity) obj;

			return this.x == loc2.x && this.y == loc2.y;
		}
		return false;
	}

	public static LocationEntity from(Coordinate coordinate) {
		return new LocationEntity(coordinate.x, coordinate.y);
	}
}
