/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.map;

import tk.risingnations.server.game.common.Coordinate;
import tk.risingnations.server.game.territory.TerrainType;
import tk.risingnations.server.support.beans.Bean;

@Bean
public interface IMapGenerationService {
	TerrainType generateTerrainType(LocationEntity location);

	TerrainType generateTerrainType(Coordinate coordinate);

	TerrainType generateTerrainType(int x, int y);

}
