package tk.risingnations.server.game.map;

import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.support.beans.Bean;

@Bean
public interface IMapService {

	boolean hasTown(int x, int y) throws ProcessingException;

	boolean hasTown(LocationEntity location) throws ProcessingException;

	LocationEntity getRandomCoordinate(boolean excludeTowns) throws ProcessingException;
}
