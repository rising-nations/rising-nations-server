package tk.risingnations.server.game.map;

import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.game.town.TownEntity;

// TODO [skeeks] generate the map dynamically with noise algorithm
public class MapService implements IMapService {

	@Override
	public boolean hasTown(int x, int y) throws ProcessingException {
		return TownEntity.findByLocation(new LocationEntity(x, y)) != null;
	}

	@Override
	public boolean hasTown(LocationEntity location) throws ProcessingException {
		return hasTown(location.getX(), location.getY());
	}

	public LocationEntity getRandomCoordinate(boolean excludeTowns) throws ProcessingException {
		if (excludeTowns) {
			return getRandomCoordinateWithoutTown();
		} else {
			// TODO [skeeks]
			return null;
		}
	}

	protected LocationEntity getRandomCoordinateWithoutTown() throws ProcessingException {
		// TODO [skeeks]
		// generate 10 random coordinates. then check on the db, if they are occupied.
		// return the first coordinate which is not occupied
		// wrap this in a loop
		return null;
	}
}
