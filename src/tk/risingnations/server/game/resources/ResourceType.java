/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.resources;

/**
 * All kind of resources.
 * @author skeeks
 *
 */
public enum ResourceType {
	WHEAT("Wheat"),
	OIL("Oil"),
	STEEL("Steel"),
	ORE("Ore"),
	WORKERS("Workers"),
	GOLD("Gold");
	
	private String m_textKeyword;
	
	ResourceType(String textKeyword){
		this.m_textKeyword = textKeyword;
	}
	
	@Override
	public String toString() {
		return m_textKeyword;
	}
	
}
