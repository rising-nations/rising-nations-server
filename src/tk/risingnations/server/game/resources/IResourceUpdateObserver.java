/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.resources;

public interface IResourceUpdateObserver {
	/**
	 * This method is called on each resource update.
	 * 
	 * @param townResources
	 */
	abstract void onResourceUpdate(ResourcesEntity townResources);
}
