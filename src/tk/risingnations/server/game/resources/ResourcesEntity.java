/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.resources;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import tk.risingnations.server.database.IEntity;

@Entity(noClassnameStored = true)
public class ResourcesEntity implements IEntity {
	private static final long serialVersionUID = 1L;
	@Id
	private ObjectId id;

	private long wheat;
	private long oil;
	private long steel;
	private long ore;
	private long workers;
	private long gold;

	public ResourcesEntity() {

	}

	public ResourcesEntity(ResourcesEntity resources) {
		this.wheat = resources.wheat;
		this.oil = resources.oil;
		this.steel = resources.steel;
		this.ore = resources.ore;
		this.workers = resources.workers;
		this.gold = resources.workers;
	}

	public ResourcesEntity(long wheat, long oil, long steel, long ore, long workers, long gold) {
		this.wheat = wheat;
		this.oil = oil;
		this.steel = steel;
		this.ore = ore;
		this.workers = workers;
		this.gold = workers;
	}

	@Override
	public ObjectId getId() {
		return id;
	}

	@Override
	public void setId(ObjectId id) {
		this.id = id;
	}

	public long getWheat() {
		return wheat;
	}

	public long getOil() {
		return oil;
	}

	public long getSteel() {
		return steel;
	}

	public long getOre() {
		return ore;
	}

	public long getWorkers() {
		return workers;
	}

	public long getGold() {
		return gold;
	}

	/**
	 *
	 * Add methods: Add amount to a resource.
	 *
	 */

	public void addWheat(long wheat) {
		this.wheat += wheat;
	}

	public void addOil(long oil) {
		this.oil += oil;
	}

	public void addSteel(long steel) {
		this.steel += steel;
	}

	public void addOre(long ore) {
		this.ore += ore;
	}

	public void addWorkers(long workers) {
		this.workers += workers;
	}

	public void addGold(long gold) {
		this.gold += gold;
	}

	public void add(ResourcesEntity resources) {
		if (resources == null) {
			return;
		}
		this.wheat += resources.wheat;
		this.oil += resources.oil;
		this.ore += resources.ore;
		this.steel += resources.steel;
		this.workers += resources.workers;
		this.gold += resources.gold;
	}

	/**
	 *
	 * Sub methods: Subtract amount to a resource.
	 *
	 */

	public void subWheat(long wheat) {
		this.wheat -= wheat;
	}

	public void subOil(long oil) {
		this.oil -= oil;
	}

	public void subSteel(long steel) {
		this.steel -= steel;
	}

	public void subOre(long ore) {
		this.ore -= ore;
	}

	public void subWorkers(long workers) {
		this.workers -= workers;
	}

	public void subGold(long gold) {
		this.gold -= gold;
	}

	public void sub(ResourcesEntity resources) {
		if (resources == null) {
			return;
		}
		this.wheat -= resources.wheat;
		this.oil -= resources.oil;
		this.ore -= resources.ore;
		this.steel -= resources.steel;
		this.workers -= resources.workers;
		this.gold -= resources.gold;
	}

	/**
	 * Increase methods. Increase resource with a factor. Example: There is
	 * already 1000 wheat there. this.multiplyWheat(1.3) will multiply the wheat
	 * with 1.3, so you have now 1300 wheat.
	 */

	public void multiplyWheat(float factor) {
		this.wheat *= factor;
	}

	public void multiplyOil(float factor) {
		this.oil *= factor;
	}

	public void multiplySteel(float factor) {
		this.steel *= factor;
	}

	public void multiplyOre(float factor) {
		this.ore *= factor;
	}

	public void multiplyWorkers(float factor) {
		this.workers *= factor;
	}

	public void multiplyGold(float factor) {
		this.gold *= factor;
	}
}
