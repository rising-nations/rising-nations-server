/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.building;

/**
 * This enums contains for each building a entry.
 * @author skeeks
 *
 */
public enum BuildingType {
	REFINERY("Refinery"),
	FARM("Farm"),
	OREMINE("Oremine"),
	STEELFACTORY("SteelFactory"),
	CITY_BLOCK("CityBlock"),
	COMMANDO_CENTER("CommandoCenter"),
	SURVEILLANCEHUB("Surveillancehub"),
	ARSENAL("Arsenal"),
	AIRPORT("Airport"),
	RESEARCH_CENTER("ResearchCenter"),
	NAVY_HARBOR("NavyHarbor");
	
	
	private String m_textKeyword;
	
	BuildingType(String textKeyword){
		this.m_textKeyword = textKeyword;
	}
	
	@Override
	public String toString() {
		return m_textKeyword;
	}
}
