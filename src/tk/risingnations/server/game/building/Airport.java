/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.building;

import tk.risingnations.server.game.army.ArmyUnitEducation;
import tk.risingnations.server.game.resources.ResourcesEntity;

public class Airport extends AbstractBuilding {

	public Airport(int level) {
		super(level);
	}

	@Override
	String getConfiguredTitleKeyword() {
		return "AirportBuildingTitle";
	}

	@Override
	String getConfiguredDescriptionKeyword() {
		return "AirportBuildingDesc";
	}

	@Override
	public void onArmyUnitEducation(ArmyUnitEducation troopEducation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onResourceUpdate(ResourcesEntity townResources) {
		// TODO Auto-generated method stub
		
	}


}
