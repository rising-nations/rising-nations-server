/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.building;

public interface IBuildingConstructionObserver {
	/**
	 * This method is called everytime, when a new building is going to be
	 * "constructed".
	 * 
	 */
	abstract void onBuildingConstruction(BuildingConstruction troopEducation);
}
