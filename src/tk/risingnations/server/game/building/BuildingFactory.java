/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.building;

import tk.risingnations.server.errorhandling.ProcessingException;

public class BuildingFactory {

	public AbstractBuilding createBuilding(BuildingType type, int level) throws ProcessingException{
		if(type == null){
			throw new ProcessingException(new NullPointerException("building type cannot be null"));
		}
		
		switch(type){
			case AIRPORT:
				return createAirport(level);
			default:
				throw new ProcessingException("found now building for building type " + type.toString());
		}
	}
	
	public Airport createAirport(int level){
		return new Airport(level);
	}
}
