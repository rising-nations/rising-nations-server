/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.building;

import tk.risingnations.server.game.army.IArmyUnitEducationObserver;
import tk.risingnations.server.game.resources.IResourceUpdateObserver;

public abstract class AbstractBuilding implements IArmyUnitEducationObserver, IResourceUpdateObserver {
	private int level;
	
	public AbstractBuilding(int level){
		if(level < 0){
			level = 0;
		}
		
		this.level = level;
	}
	
	public int getLevel() {
		return level;
	}
	
	/**
	 * Title of of this building. (only the keyword for the TEXTS_*.json file)
	 */
	abstract String getConfiguredTitleKeyword();
	
	/**
	 * Description text of this building.  (only the keyword for the TEXTS_*.json file)
	 */
	abstract String getConfiguredDescriptionKeyword();

}
