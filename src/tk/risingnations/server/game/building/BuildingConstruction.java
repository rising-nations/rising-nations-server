/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.building;

import java.time.Duration;

import tk.risingnations.server.game.resources.ResourcesEntity;

public class BuildingConstruction extends ResourcesEntity {

	private static final long serialVersionUID = 1L;
	
	private Duration m_buildingDuration;
	
	public BuildingConstruction(long wheat, long oil, long steel, long ore, long workers, long gold) {
		super(wheat, oil, steel, ore, workers, gold);
		// TODO Auto-generated constructor stub
	}

	public Duration getBuildingDuration() {
		return m_buildingDuration;
	}
	
	public void addBuildingDuration(Duration duration){
		if(duration != null){
			this.m_buildingDuration.plus(duration);
		}
	}
	
	public void subBuildingDuration(Duration duration){
		if(duration != null){
			this.m_buildingDuration.minus(duration);
		}
	}
	
	public void multiplyBuildingDuration(float factor){
		this.m_buildingDuration = Duration.ofNanos(Math.round(this.m_buildingDuration.toNanos() * factor));
	}
}
