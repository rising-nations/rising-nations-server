/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.army;

public interface IArmyDeploymentObserver {
	/**
	 * The armyDeployment describes the marsh duration, the resources they transport.
	 */
	void onArmyDeployment(ArmyDeployment armyDeployment);
}
