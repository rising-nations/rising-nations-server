/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.army;

import tk.risingnations.server.errorhandling.ProcessingException;

public class ArmyUnitFactory {
	
	public AbstractArmyUnit createArmyUnit(ArmyUnitType type, long amount) throws ProcessingException{
		if(type == null){
			throw new ProcessingException(new NullPointerException("army unit type cannot be null"));
		}
		
		switch(type){
			case INFANTRY:
				return createInfantry(amount);
			default:
				throw new ProcessingException("found no army unit for army unit type " + type.toString());
		}
	}
	
	public Infantry createInfantry(long amount){
		return new Infantry(amount);
	}
}
