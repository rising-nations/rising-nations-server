/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.army;

import tk.risingnations.server.game.resources.IResourceUpdateObserver;

public abstract class AbstractArmyUnit implements IResourceUpdateObserver {
	private long amount;
	
	public AbstractArmyUnit(long amount) {
		this.amount = amount;
	}
	
	public long getAmount() {
		return amount;
	}
}
