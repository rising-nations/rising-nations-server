/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.army;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

import tk.risingnations.server.database.IEntity;
import tk.risingnations.server.game.map.LocationEntity;
import tk.risingnations.server.game.player.PlayerEntity;
import tk.risingnations.server.game.resources.ResourcesEntity;

@Entity(noClassnameStored = true)
public class ArmyEntity implements IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	private ObjectId id;

	@Embedded
	private LocationEntity origin;

	@Embedded
	private LocationEntity destination;

	@Reference
	private PlayerEntity player;

	@Embedded
	private List<ArmyUnitEntity> armyUnits;

	@Embedded
	private List<ResourcesEntity> resourceCargos;

	@Override
	public ObjectId getId() {
		return id;
	}

	@Override
	public void setId(ObjectId id) {
		this.id = id;
	}

	public LocationEntity getOrigin() {
		return origin;
	}

	public LocationEntity getDestination() {
		return destination;
	}

	public PlayerEntity getPlayer() {
		return player;
	}

	public List<ArmyUnitEntity> getArmyUnits() {
		return armyUnits;
	}

	public List<ResourcesEntity> getResourceCargo() {
		return resourceCargos;
	}

	public void setDestination(LocationEntity destination) {
		this.destination = destination;
	}

	public void setOrigin(LocationEntity origin) {
		this.origin = origin;
	}

	public void setPlayer(PlayerEntity player) {
		this.player = player;
	}

	public void setResourceCargo(ResourcesEntity resourceCargo) {
		if (resourceCargo == null) {
			resourceCargos = new ArrayList<ResourcesEntity>();
		}
		this.resourceCargos.add(resourceCargo);
	}

	public void setArmyUnits(ArmyUnitEntity armyUnit) {
		if (armyUnits == null) {
			armyUnits = new ArrayList<ArmyUnitEntity>();
		}
		this.armyUnits.add(armyUnit);
	}
}
