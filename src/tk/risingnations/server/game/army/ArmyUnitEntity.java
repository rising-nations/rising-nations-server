/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.army;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import tk.risingnations.server.database.IEntity;

@Entity(noClassnameStored = true)
public class ArmyUnitEntity implements IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	private ObjectId id;

	private ArmyUnitType type;

	private long amount;

	@Override
	public void setId(ObjectId id) {
		this.id = id;

	}

	@Override
	public ObjectId getId() {
		return this.id;
	}

	public ArmyUnitEntity(ArmyUnitType type, long amount) {
		this.type = type;
		this.amount = amount;
	}

	public ArmyUnitType getType() {
		return type;
	}

	public long getAmount() {
		return amount;
	}
}
