/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.army;

public interface IArmyUnitEducationObserver {
	/**
	 * This method is called everytime, when a new army is going to be
	 * "produced".
	 * 
	 * @param armyEducation
	 */
	abstract void onArmyUnitEducation(ArmyUnitEducation troopEducation);
}
