/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.army;

import java.time.Duration;
import java.util.List;

import tk.risingnations.server.game.resources.ResourcesEntity;

public class ArmyDeployment extends ResourcesEntity {
	private Duration m_marshTime;
	private List<AbstractArmyUnit> armyUnits;

	private static final long serialVersionUID = 1L;

	public Duration getMarshTime() {
		return m_marshTime;
	}

	public void addMarshTime(Duration duration) {
		if (duration != null) {
			m_marshTime.plus(duration);
		}
	}
	
	public void subMarshTime(Duration duration) {
		if (duration != null) {
			m_marshTime.minus(duration);
		}
	}
	
	public void multiplyMarshTime(float factor) {
		this.m_marshTime = Duration.ofNanos(Math.round(this.m_marshTime.toNanos() * factor));
	}
	
	public List<AbstractArmyUnit> getArmyUnits() {
		return armyUnits;
	}
}
