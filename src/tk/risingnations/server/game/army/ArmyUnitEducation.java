/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.army;

import java.time.Duration;

import tk.risingnations.server.game.resources.ResourcesEntity;

/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
public class ArmyUnitEducation extends ResourcesEntity {
	private static final long serialVersionUID = 1L;
	
	
	private final ArmyUnitType type;
	private final long amount;
	private Duration educationDuration;
	
	public ArmyUnitEducation(ArmyUnitType type, long amount, Duration time, ResourcesEntity resources) {
		super(resources);
		this.type = type;
		this.amount = amount;
		this.educationDuration = time;
	}
	
	public ArmyUnitType getType() {
		return type;
	}
	
	public long getAmount() {
		return amount;
	}

	public Duration getEducationDuration() {
		return educationDuration;
	}
	
	public void addEducationDuration(Duration duration){
		this.educationDuration.plus(duration);
	}
	
	public void subEducationDuration(Duration duration){
		this.educationDuration.minus(duration);
	}
	
	public void multiplyDuration(float factor){
		this.educationDuration = Duration.ofNanos(Math.round(this.educationDuration.getNano() * factor));
	}
}
