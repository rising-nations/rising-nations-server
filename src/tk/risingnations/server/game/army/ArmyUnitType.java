/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.army;

/**
 * This enum contains all differenty army unit types.
 * @author skeeks
 *
 */
public enum ArmyUnitType {
	INFANTRY("Infantry"),
	MECH_INFANTY("MechanisedInfantry"),
	TRUCK("Truck"),
	LIGHT_TANK("LighTank"),
	MEDIUM_HEAVY_TANK("MediumHeavyTank"),
	HEAVY_TANK("HeavyTank"),
	ANTI_AIRCRAFT_TANK("AntiAircraftTank"),
	TANK_HOWITZER("TankHowitzer"),
	ROCKET_LAUNCHER("RocketLauncher"),
	ATTACK_AIRCRAFT("AttackAircraft"),
	TRANSPORTING_AIRCRAFT("TransportingAircraft"),
	SURVEILLANCE_AIRCRAFT("SurveillanceAircraft"),
	FIGHTING_AIRCRAFT("FightingAircraft"),
	BOMBER("Bomber"),
	LIGHT_SUBMARINE("LightSubmarine"),
	HEAVY_SUBMARINE("HeavySubmarine"),
	DESTROYER("Destroyer"),
	CRUISER("Cruiser"),
	BATTLESHIP("Battleship");
	
	
	private String m_textKeyword;
	
	ArmyUnitType(String textKeyword){
		this.m_textKeyword = textKeyword;
	}
	
	@Override
	public String toString() {
		return m_textKeyword;
	}
}
