/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.town;

import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

import tk.risingnations.server.database.IEntity;
import tk.risingnations.server.database.IMongoDbService;
import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.game.building.BuildingEntity;
import tk.risingnations.server.game.map.LocationEntity;
import tk.risingnations.server.game.resources.ResourcesEntity;
import tk.risingnations.server.game.world.WorldType;
import tk.risingnations.server.support.beans.BEANS;

@Entity(noClassnameStored = true)
public class TownEntity implements IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	private ObjectId id;

	private WorldType world;

	private String name;

	@Embedded
	@Indexed
	private LocationEntity location;

	private boolean isCoastTown;

	@Embedded
	private List<BuildingEntity> buildings;

	private int allowedBuildings;

	@Embedded
	private ResourcesEntity resources;

	public String getName() {
		return name;
	}

	public LocationEntity getLocation() {
		return location;
	}

	public boolean isCoastTown() {
		return isCoastTown;
	}

	public List<BuildingEntity> getBuildings() {
		return buildings;
	}

	public ResourcesEntity getResources() {
		return resources;
	}

	public void setAllowedBuildings(int allowedBuildings) {
		this.allowedBuildings = allowedBuildings;
	}

	public void addBuilding(BuildingEntity building) {
		this.buildings.add(building);
	}

	public void removeBuilding(BuildingEntity building) {
		this.buildings.remove(building);
	}

	public void setCoastTown(boolean isCoastTown) {
		this.isCoastTown = isCoastTown;
	}

	public void setLocation(LocationEntity location) {
		this.location = location;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setResources(ResourcesEntity resources) {
		this.resources = resources;
	}

	public int getAllowedBuildings() {
		return allowedBuildings;
	}

	public WorldType getWorld() {
		return world;
	}

	public void setWorld(WorldType world) {
		this.world = world;
	}

	public static TownEntity findByLocation(LocationEntity location) throws ProcessingException {
		TownEntity town = BEANS.get(IMongoDbService.class).query(TownEntity.class).field("location").equal(location).get();
		return town;
	}

	@Override
	public ObjectId getId() {
		return id;
	}

	@Override
	public void setId(ObjectId id) {
		this.id = id;
	}
}
