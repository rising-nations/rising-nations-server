package tk.risingnations.server.game.town;

import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.game.map.IMapService;
import tk.risingnations.server.game.map.LocationEntity;
import tk.risingnations.server.support.beans.BEANS;
import tk.risingnations.server.tools.dev.Stopwatch;

public class TownGenerator {
	public static TownEntity generate() throws ProcessingException {
		IMapService mapSvc = BEANS.get(IMapService.class);
		TownEntity town = new TownEntity();
		{
			Stopwatch.start();
			LocationEntity location = mapSvc.getRandomCoordinate(true);
			Stopwatch.stop("random coordinate without town");
			town.setLocation(location);
		}
		return town;
	}
}
