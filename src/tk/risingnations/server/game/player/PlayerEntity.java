/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.player;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

import tk.risingnations.server.account.AccountEntity;
import tk.risingnations.server.database.IEntity;
import tk.risingnations.server.database.IMongoDbService;
import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.game.research.ResearchEntity;
import tk.risingnations.server.game.town.TownEntity;
import tk.risingnations.server.game.world.WorldType;
import tk.risingnations.server.support.beans.BEANS;

@Entity(value = "players", noClassnameStored = true)
public class PlayerEntity implements IEntity {

	private static final long serialVersionUID = 1L;

	@Id
	private ObjectId id;

	private WorldType world;

	private String username;

	@Reference
	private AccountEntity account;

	@Reference
	private List<TownEntity> towns = new ArrayList<TownEntity>();

	private int currentTownIndex;

	private long reputation;

	@Embedded
	private List<ResearchEntity> researchs;

	public long getReputation() {
		return reputation;
	}

	public String getUsername() {
		return username;
	}

	public WorldType getWorld() {
		return world;
	}

	@Override
	public ObjectId getId() {
		return id;
	}

	@Override
	public void setId(ObjectId id) {
		this.id = id;
	}

	public AccountEntity getAccount() {
		return account;
	}

	public List<TownEntity> getTowns() {
		return towns;
	}

	public TownEntity getCurrentTown() {
		TownEntity currTown = towns.get(currentTownIndex);
		if (currTown == null) {
			return towns.get(0);
		}
		return towns.get(currentTownIndex);
	}

	public static PlayerEntity createById(String userId) throws ProcessingException {
		PlayerEntity player = BEANS.get(IMongoDbService.class).getDatastore().createQuery(PlayerEntity.class).field("id").equal(new ObjectId(userId)).get();
		if (player == null) {
			throw new ProcessingException("player does not exists in database. (" + userId + ")");
		}
		return player;
	}

	public List<ResearchEntity> getResearchs() {
		return researchs;
	}

	public boolean hasTowns() {
		return getTowns().size() > 0;
	}

	public void setWorld(WorldType world) {
		this.world = world;
	}

	public void setAccount(AccountEntity account) {
		this.account = account;
	}

	public void setCurrentTownIndex(int currentTownIndex) {
		this.currentTownIndex = currentTownIndex;
	}

	public void setReputation(long reputation) {
		this.reputation = reputation;
	}

	public void setResearchs(ResearchEntity research) {
		if (this.researchs == null) {
			this.researchs = new ArrayList<ResearchEntity>();
		}
		this.researchs.add(research);
	}
}
