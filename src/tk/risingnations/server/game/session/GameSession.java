package tk.risingnations.server.game.session;

import tk.risingnations.server.game.player.PlayerEntity;
import tk.risingnations.server.game.town.TownEntity;

public class GameSession {
	// STATIC PART
	private final static ThreadLocal<GameSession> SESSIONS = new ThreadLocal<>();

	public static GameSession get() {
		GameSession session = SESSIONS.get();
		if (session == null) {
			throw new RuntimeException("no game-session found");
		}
		return session;
	}

	public static void put(GameSession session) {
		SESSIONS.set(session);
	}

	private GameSession() {

	}

	public static GameSession create(String userId) {
		return new GameSession();
	}

	public PlayerEntity getPlayer() {
		return null;
	}

	public TownEntity getTown() {
		return null;
	}
}
