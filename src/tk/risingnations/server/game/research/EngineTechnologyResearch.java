/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.research;

import tk.risingnations.server.game.army.ArmyUnitEducation;
import tk.risingnations.server.game.building.BuildingConstruction;
import tk.risingnations.server.game.resources.ResourcesEntity;

public class EngineTechnologyResearch extends AbstractResearch {

	public EngineTechnologyResearch(int level) {
		super(level);
	}

	@Override
	String getConfiguredTitleKeyword() {
		return "EngineTechnologyTitle";
	}

	@Override
	String getConfiguredDescriptionKeyword() {
		return "EngineTechnologyDescription";
	}

	@Override
	public void onArmyUnitEducation(ArmyUnitEducation troopEducation) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onResourceUpdate(ResourcesEntity townResources) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBuildingConstruction(BuildingConstruction troopEducation) {
		// TODO Auto-generated method stub
	}

}
