/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.research;

import tk.risingnations.server.errorhandling.ProcessingException;

public class ResearchFactory {
	public AbstractResearch createResearch(ResearchType type, int level) throws ProcessingException{
		if(type == null){
			throw new ProcessingException(new NullPointerException("research type cannot be null"));
		}
		
		switch(type){
			case ENGINE_TECHNOLOGY:
				return createEngineTechnologyResearch(level);
			default:
				throw new ProcessingException("found no research for research type " + type.toString());
		}
	}
	
	public AbstractResearch createEngineTechnologyResearch(int level) {
		return new EngineTechnologyResearch(level);
	}
}
