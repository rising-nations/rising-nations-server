package tk.risingnations.server.game.research;



import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity(noClassnameStored = true)
public class ResearchEntity {

	@Id
	private ObjectId id;

	private int level;
	private ResearchType type;

	public ResearchEntity(ResearchType type, int level) {
		this.type = type;
		this.level = level;
	}

	public int getLevel() {
		return level;
	}

	public ResearchType getType() {
		return type;
	}
}
