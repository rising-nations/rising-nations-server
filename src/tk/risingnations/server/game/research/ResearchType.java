/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.research;

public enum ResearchType {
	WHEAT_FARMING("WheatFarming"), 
	FORGING_TECHNIQUES("ForgingTechniques"), 
	MINING_TECHNIQUES("MiningTechniques"), 
	DRILLING_TECHNIQUES("DrillingTechniques"), 
	LIFE_QUALITY("LifeQuality"), 
	CONSTRUCTION_TECHNIQUES("ConstructionTechniques"), 
	ENGINE_TECHNOLOGY("EngineTechnology");
	
	private String m_textKeyword;
	
	ResearchType(String textKeyword){
		this.m_textKeyword = textKeyword;
	}
	
	@Override
	public String toString() {
		return this.m_textKeyword;
	}
}
