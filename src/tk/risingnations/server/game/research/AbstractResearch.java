/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.game.research;

import tk.risingnations.server.game.army.IArmyUnitEducationObserver;
import tk.risingnations.server.game.building.IBuildingConstructionObserver;
import tk.risingnations.server.game.resources.IResourceUpdateObserver;

public abstract class AbstractResearch implements IArmyUnitEducationObserver, IResourceUpdateObserver, IBuildingConstructionObserver {
	private int m_level;

	public AbstractResearch(int level) {
		this.m_level = level;
	}

	public final int getLevel() {
		return m_level;
	}

	abstract String getConfiguredTitleKeyword();

	abstract String getConfiguredDescriptionKeyword();

}
