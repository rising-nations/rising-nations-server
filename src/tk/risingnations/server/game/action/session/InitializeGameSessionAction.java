package tk.risingnations.server.game.action.session;

import tk.risingnations.server.action.common.GameAction;
import tk.risingnations.server.action.common.GameHandler;
import tk.risingnations.server.action.common.IGameAction;
import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.game.session.GameSession;
import tk.risingnations.server.game.town.TownEntity;
import tk.risingnations.server.game.town.TownGenerator;

/**
 * This action initializes a game session.
 * A GameSession is basically opening the game in the browser till the user closes the window / tab.
 * 
 * Tasks to do here:
 * - Load Player from db to cache
 * - Create a town if the user has no one yet
 *
 * @author skeeks
 *
 */
@GameAction("initialize-game")
public class InitializeGameSessionAction implements IGameAction {

	@GameHandler
	public void exec() throws ProcessingException {
		if (!GameSession.get().getPlayer().hasTowns()) {
			TownEntity town = TownGenerator.generate();
			// TODO [skeeks] generate a new town for the player
		}
	}

}
