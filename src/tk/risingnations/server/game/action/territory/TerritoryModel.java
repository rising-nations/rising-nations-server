package tk.risingnations.server.game.action.territory;

import tk.risingnations.server.game.territory.TerrainType;
import tk.risingnations.server.models.LocationModel;

public class TerritoryModel extends LocationModel {
	public TerrainType type;
	
	public TerritoryModel(int x, int y, TerrainType type) {
		super(x, y);
		this.type = type;
	}
}
