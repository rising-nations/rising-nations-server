/*
 * Copyright 2016 Samuel Keusch Samuel Eiben Jan Widmer
 */
package tk.risingnations.server.account;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.IndexOptions;
import org.mongodb.morphia.annotations.Indexed;

import tk.risingnations.server.database.IEntity;
import tk.risingnations.server.database.IMongoDbService;
import tk.risingnations.server.errorhandling.ProcessingException;
import tk.risingnations.server.support.beans.BEANS;

/**
 *
 * @author skeeks
 */
@Entity(noClassnameStored = true)
public class AccountEntity implements IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	private ObjectId id;

	@Indexed(options = @IndexOptions(unique = true))
	private String username;

	private String email;

	private String password;

	private String lastLoginDate;

	private boolean newsletterSubscribed;

	@Override
	public ObjectId getId() {
		return id;
	}

	@Override
	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getAccountId() {
		return id.toHexString();
	}

	public String getEmail() {
		return email;
	}

	public String getUsername() {
		return username;
	}

	public String getLastLoginDate() {
		return lastLoginDate;
	}

	public String getPassword() {
		return password;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isNewsletterSubscribed() {
		return newsletterSubscribed;
	}

	public void setNewsletterSubscribed(boolean newsletterSubscribed) {
		this.newsletterSubscribed = newsletterSubscribed;
	}

	public static AccountEntity loadByUsername(String username) throws ProcessingException {
		AccountEntity entity = BEANS.get(IMongoDbService.class).query(AccountEntity.class).field("username").equal(username).get();
		return entity;
	}
}
