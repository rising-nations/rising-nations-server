package tk.risingnations.server.models;

public class LocationModel {

	public int x;
	public int y;

	public LocationModel(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
