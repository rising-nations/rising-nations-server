package tk.risingnations.server.models;

import java.util.Date;

import tk.risingnations.server.game.building.BuildingType;

public class BuildingUnderConstructionModel {
	public BuildingType type;
	
	public Date evtFinish;
	
	public int toLevel;
}
