package tk.risingnations.server.models;

import java.util.List;

import tk.risingnations.server.game.map.LocationEntity;

public class TownModel {
	public String name;

	public int moral;

	public LocationEntity location;

	public List<RunningResearchModel> researches;

	public List<BuildingUnderConstructionModel> buildingsUnderConstruction;

	public List<ResourceModel> resources;
}
