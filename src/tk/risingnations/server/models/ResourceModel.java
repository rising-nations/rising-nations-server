package tk.risingnations.server.models;

import tk.risingnations.server.game.resources.ResourceType;

public class ResourceModel {
	public ResourceType type;
	
	public long amount;
	
	public long productionPerHour;

}
