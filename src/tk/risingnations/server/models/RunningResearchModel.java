package tk.risingnations.server.models;

import java.util.Date;

import tk.risingnations.server.game.research.ResearchType;

public class RunningResearchModel {
	
	public ResearchType type;
	
	public Date evtFinish;
	
	public int toLevel;
}
