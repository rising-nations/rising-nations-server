package tk.risingnations.server.models;

import java.util.Date;

import tk.risingnations.server.game.army.ArmyUnitType;

public class ArmyUnitInEducationModel {
	public ArmyUnitType type;
	
	public Date evtFinish;
	
	public long amount;
}
